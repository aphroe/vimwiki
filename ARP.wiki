= ARP =
Performs a translation from a logical Internet (Network) address to a phyiscal hardware address. ARP has it's own header format.
Data Link Layer NICs such as Ethernet Cards don't understand IP Adresses. 
It needs the Ethernet (MAC) Adress of a destination host to send the frame.
For this purpose ARP (Adress Resolution Protocol) is used. This is a common example for direct delivery (hosts on the same subnet):

1. A sender wants to send a packet to 192.168.0.50.
2. Before putting the data on the wire the IP packet needs to be encapsulated in an Ethernet frame.
   So the sender needs the destination MAC Address.
4. It broadcasts an ARP request on the layer 2 Ethernet network (FF:FF:FF:FF:FF), asking which MAC Address belongs to 192.168.0.50.
   (Who owns IP Address 192.168.0.50?)
6. If the destination host is alive and on the same network, it will send an ARP response back to the orginial sender with its Ethernet Address.
7. The sender takes the MAC Adress from the arp response and forges the frame. The frame can now be transmitted.

ARP replies and requests are cached on hosts and switches, therefore decreasing the load on the network. 

If a destination host is on another subnet or on a complete different network (maybe other side of the planet), the sender makes an ARP request for the gateway matched in the routing table (for hosts almost always the default gateway). It then uses the MAC Address it got from the router to setup the frame and sends it. If the router gets the packet, it uses arp as well to determine the next link layer destination.
If the destination is again on another network, the procedure gets repeated.

== Gratious ARP ==
If a host sends an ARP request asking who has it's own IP Address, is called *Gratious ARP*. This often happens if a host gets turned on. This is done for 2 reasons:

1. If a another host responds with an arp reply, it will know that there is another host that is already configured with the same IP Address.
   Often an error message is shown to the user.
3. It refreshes the ARP chache of all hosts in the broadcast domain if it's hardware address was changed.
