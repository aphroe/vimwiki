= C++ Basics =
== Types ==
Types tell us what our data means and what operations we can perfom on those data. There are builtin and class types.

== Objects ==
Generally speaking an object is a region in memory which contains data and has a type.

== Literals == 
Literals are objects that are self-evident. They have no names and are `rvalues`.

Example: `"str"` or `42`

== Initialization ==
Objects which are not initialized with an initializer are default initialized.

=== Default Initialization ===
Global space:
Objects which are default initialized in global scope are value-initialized. They have a valid state.

The behaviour in local scope depends on the type:
Objects of builtin types are uninitialized.
Using uninitialized objects in expression or copying them results in undefined behaviour!
Class types are initialized with the default constructor. The state after such an intialization depends on the class designer.
Usually class types initializes their member in a state which is safe to use. This is the case for all Standard Library class types.
If a class type has no default constructor and is not initialized with an intializer results in an error.

Types of Initialization:
Direct Initialization
Copy Initialization
List Intialization
Default Intialization

== Declaration and Definition ==
A declaration makes a name known to the program. A defintion creates the associated memory.

== Scope ==
