= IP Forwarding =
Conceptually, IP forwarding is simple, especially for a host. If the destination is directly connected to the host (e.g. a point-to-point link) or on a shared network (e.g. Ethernet), the IP datagram is sent directly to the destination - a router is not required or used.
Otherwise, the host sends the datagram to a single router (called the **default** router) and lets the router deliver the datagram to its destination.
In our general scheme, the IP protocol can recieve a datagram either from another protocol on the same machine (UDP, TCP, etc.) or from a network interface. The IP layer has some information in memory, usually called a routing table or forwarding table, which it searches each time it recieves a datagram to send.

== Forwarding table ==
Key pieces of information that are generally required to implement a forwarding table:

- *Destination*: Contains a 32 bit address (for IPv4, 128 bit for IPv6) used for matching the result of a masking operation.
  The destination can be as simple as zero, for a "default route" covering all destinations, or as long as the full length of an IP address,
  in the case of a "host route" that describes only a single destination.
  
- *Mask*: This contains a 32 bit field (128 bit for IPv6) applied as a bitwise AND mask to the destination IP address of a datagram being looked up  in the forwarding table. The masked result is compared with the set of destinations in the forwarding table entries.

- *Next-hop*: This contains the 32 bit IPv4 address or 128 bit IPv6 address of the next IP entity (router or host) to which the datagram should be   sent. The next hop entity is typically on a network shared with the system performing the forwarding lookup, meaning the two share the same
  network prefix.
  
- *Interface*: This contains an identifier used by the IP layer to reference the network interface that should be used to send the datagram
  to its next hop. For example, it could refer to a host's 802.11 wireless interface, a wired Ethernet interface, or a PPP interface associated
  with a serial port. If The forwarding system is also the sender of the IP datagram, this field is used in selecting which source IP address to
  use on the outgoing datagram.
  
== IP Forwarding Actions ==

  
== Examples ==
=== Direct delivery ===
Sender and Destination has to be on the same network (layer 2: e.g. Ethernet) and on the same subnet.
When th IP layer of a sender recieves a datagram to send from one of the upper layers such as TCP or UDP, it searches its forwarding table.

Sender IP: 10.0.0.100/25 (S)
Destination IP: 10.0.0.9/25 (D)

Forwarding table

 | Destination | Mask            | Gateway (Next Hop) | Interface  |
 |-------------|-----------------|--------------------|------------|
 | 0.0.0.0     | 0.0.0.0         | 10.0.0.1           | 10.0.0.100 |
 | 10.0.0.0    | 255.255.255.128 | 10.0.0.100         | 10.0.0.100 |
 
The destination IP address matches both the first and second entry of the forwarding table. Because it matches the second entry better
(25 bits instead of none) the "gateway" or next hop is 10.0.0.100, the address S. The gateway portion of the sending host's own network interface,
indicating that direct delivery is to be used to send the datagram. The datagram is encapsulated in a link layer frame (e.g. Ethernet). If the
destination link layer address is unknow, the ARP protocol may be invoked to determine the correct lower layer address. The switch delivers the 
frame to D based soley on the link layer address, it pays no attention to the IP address.

=== Indirect delivery ===
